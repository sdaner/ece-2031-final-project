; Battery Test
; Ryan Reeves
;;

        ORG     &H0000      ; Begin at 0x0000
Init:   LOAD    Batcmd      ; Load the command
        OUT     I2C_CMD     ; Initialize I2C
        OUT     I2C_DATA    ; Initialize ADC
IWCMD:  IN      I2C_BUSY    ; Check if busy
        JPOS    IWCMD       ; Loop until done transmitting
        IN      I2C_DATA

Main:
        LOAD    Batcmd
        OUT     I2C_CMD     ; Tell I2C the command
        OUT     I2C_DATA    ; Write to ADC to start transmission
WCMD:   IN      I2C_BUSY    ; Check busy
        JPOS    WCMD        ; If busy, loop
        IN      I2C_DATA	; Get the voltage (0-255 value)
        STORE	Temp		; Store in Temp
		LOAD	Zero		; 0
DivRes:	STORE	TempDiv		; 0 TempDiv
		LOAD	Temp
		SUB		Ten
		JNEG    DivFin
		JZERO	DivFin
		STORE   Temp
		LOAD	TempDiv
		ADD		One
		JUMP	DivRes
DivFin: LOAD	TempDiv
		ADD		One
		OUT		LCD
HALT:   JUMP    Main

Temp:    DW     &H0000
TempDiv: DW     &H0000
One:     DW     &H0001
Four:    DW     &H0004
Ten:     DW     &H000A
Zero:    DW     &H0000

; Constant
Batcmd:  DW     &H0190
Divisor: DW     &H000A

; IO address space map
SWITCHES: EQU &H00  ; slide switches
RLEDS:    EQU &H01  ; red LEDs
SEVENSEG: EQU &H04  ; seven-segment display (4-digits only)
LCD:      EQU &H06  ; primitive 4-digit LCD display
I2C_CMD:  EQU &H90  ; I2C module's CMD register,
I2C_DATA: EQU &H91  ; ... DATA register,
I2C_BUSY: EQU &H92  ; ... and BUSY register

; Battery Test
; Ryan Reeves
;;

        ORG     &H0000      ; Begin at 0x0000
Init:   LOAD    Three
		OUT     RLEDS
		OUT     GLEDS
HALT:   JUMP    HALT

; Constant
Batcmd: DW      &H0190
Three:   DW      &H0003

; IO address space map
SWITCHES: EQU &H00  ; slide switches
RLEDS:    EQU &H01  ; red LEDs
SEVENSEG: EQU &H04  ; seven-segment display (4-digits only)
LCD:      EQU &H06  ; primitive 4-digit LCD display
GLEDS:    EQU &H08  ; green LEDs
I2C_CMD:  EQU &H90  ; I2C module's CMD register,
I2C_DATA: EQU &H91  ; ... DATA register,
I2C_BUSY: EQU &H92  ; ... and BUSY register

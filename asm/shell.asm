ORG     &H000           ;Begin program at x000
TestInOut:
        LOAD    FFFF
        OUT     LEDS
        LOAD    ZERO
        IN      LEDS
        SUB     FFFF
        JZERO   IOGood
        LOAD    ErrorIO
        OUT     SEVENSEG
IOGood:
        LOAD    ZERO
        OUT     TIMER
Flash:
        IN      FFFF
        OUT     LEDS
        LOAD    Ten
        STORE   WaitTime
        CALL    Wait
FinishFlash:
        LOAD    Zero
        OUT     LEDS
        LOAD    Two
        STORE   WaitTime
        LOAD    Zero
TestDisplays:
        ;0
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;1
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;2
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;3
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;4
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;5
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;6
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;7
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;8
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;9
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;A
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;B
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;C
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;D
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;E
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        ;F
        OUT     SEVENSEG
        OUT     LCD
        ADD     X1111
        CALL    Wait
        JUMP    Main
Main:
        LOAD    Prompt
        OUT     LCD
        LOAD    Ten
        STORE   WaitTime
        CALL    Wait
        IN      SWITCHES
        AND     SWSound
        JPOS    BeepInDepthTest
        IN      SWITCHES
        AND     SWSonar
        JPOS    SonarInDepthTest
        IN      SWITCHES
        AND     SWMotors
        JPOS    MotorsInDepthTest
        JUMP    Main
Done:
        JUMP    Done
        
        
SonarInDepthTest:
        LOAD    Zero
        OUT             LCD
        OUT             SEVENSEG
        LOAD    Counter
        OUT             LEDS
        OUT             SONAREN
SRead:          
        IN              SWITCHES
        AND    SWSonar
        JPOS   SRead2
        LOAD    Zero
        OUT             SONAREN
        OUT             LEDS
        OUT             LCD
        OUT             SEVENSEG
        RETURN
SRead2:         
        LOAD    Counter ;update distance on LCD
        XOR             EnSonar0
        JPOS    Snext1
        IN              DIST0   ;Sonar 0 enabled
        JUMP    Sleave
Snext1:         
        LOAD    Counter
        XOR             EnSonar1
        JPOS    Snext2
        IN              DIST1   ;Sonar 1 enabled
        JUMP    Sleave
Snext2:         
        LOAD    Counter
        XOR             EnSonar2
        JPOS    Snext3
        IN              DIST2   ;Sonar 2 enabled
        JUMP    Sleave
Snext3:         
        LOAD    Counter
        XOR             EnSonar3
        JPOS    Snext4
        IN              DIST3   ;Sonar 3 enabled
        JUMP    Sleave
Snext4:         
        LOAD    Counter
        XOR             EnSonar4
        JPOS    Snext5
        IN              DIST4   ;Sonar 4 enabled
        JUMP    Sleave
Snext5:         
        LOAD    Counter
        XOR             EnSonar5
        JPOS    Snext6
        IN              DIST5   ;Sonar 5 enabled
        JUMP    Sleave
Snext6:         
        LOAD    Counter
        XOR             EnSonar6
        JPOS    Snext7
        IN              DIST6   ;Sonar 6 enabled
        JUMP    Sleave
Snext7:         
        IN              DIST7   ;Sonar 7 enabled
Sleave:         
        OUT             SEVENSEG
        STORE   STemp
        SUB             Max             ;actual > max
        JPOS    Sfail
        LOAD    STemp
        SUB             Min             ;actual < min
        JNEG    Sfail
        LOAD    One
        OUT             LCD
        LOAD    EnSonars
        OUT             GLEDS
        JUMP    SKey
Sfail:          
        LOAD    Zero                    
        OUT     GLEDS
SKey:           
        IN              XIO                     ;Read in if SKey1 is pressed
        SUB             Key1    
        JNEG    SRead           ;If it is, wait until it's released
        JPOS    SRead
SPressed:       
        IN              XIO
        SUB             NOKEY
        JZERO   SCount          ;Once it has been released...
        JUMP    SPressed
SCount:         
        LOAD    Counter         ;Change LED to reflect correct sonar
        SHIFT   1
        STORE   Counter
        SHIFT   -8
        JZERO   SOkay
        LOAD    EnSonar0
        STORE   Counter
SOkay:          
        LOAD    Counter
        OUT             LEDS
        OUT             SONAREN
        JUMP    SRead
        
BeepInDepthTest:
        LOAD    Three			 ;Loads a value of three for the amount of seconds
        STORE   TestS			 ;Stores the value of three in TestS
READ:   IN  			SWITCHES ;Takes in active switches
		AND				SWSound  ;Makes sure the correct switches are active
        JPOS 	SOU              ;When Switch0 or Switch8 are active SOU is executed
        Load    Zero             ;Otherwise, set the sevensegment display to zero 
        OUT     		SEVENSEG
        RETURN                   ;Otherwise, stop the test
SOU:    LOAD 	TestS            ;Loads the TestS value
        OUT  			SEVENSEG ;Outputs this value to the sevensegment display
        SUB 			One      ;Subtracts one from that value
        STORE 	TestS            ;Stores the new value in TestS
        JNEG 	FINAL            ;Once the counter reaches a negative value, FINAL is executed
        OUT 			TIMER    ;Resets the timer
TIME:   IN 				TIMER	 
        SUB 			Five     ;Subtracts five from the timer to count down each second
        JNEG 	TIME             ;Waits until the proper amount of time has elapsed
        JUMP 	READ             ;Otherwise, reads in the switches again to continue the count
FINAL: IN 				SWITCHES ;Takes in the switches at the end of the test
       OUT 				LEDS     ;Lights the LED above the switch to show the test finished
       JUMP 	BeepInDepthTest  ;Resets
            
MotorsInDepthTest:
            RETURN

        
        
;===============================Wait functions==================
; Input: Store WaitTime (in tenths of a second)
Wait:
        STORE   WaitStak
        OUT     Timer
WaitCont:
        IN      Timer
        SUB     WaitTime
        JZERO   DoneWait
        JUMP    WaitCont
DoneWait:
        LOAD    WaitStak
        RETURN


; This is a good place to put variables
Temp:     DW 0
TestLED:  DW &H0
TestHex:  DW &H8888
WaitTime: DW &H01
WaitStak: DW 0
TestS:    DW 0
;
SWSound:  DW &H0101
SWSonar:  DW &H0202
SWMotors: DW &H0404
;
;SONAR VARIABLES
STemp:    DW 0
Min:      DW 300
Max:      DW 700
SonarOn:  DW 0
Counter:  DW &B00000001
KEY1:     DW &H3FFE
NOKEY:    DW &H3FFF
;SONAR VARIABLES
; Having some constants can be very useful
NegOne:   DW -1
Zero:     DW 0
One:      DW 1
Two:      DW 2
Three:    DW 3
Four:     DW 4
Five:     DW 5
Six:      DW 6
Seven:    DW 7
Eight:    DW 8
Nine:     DW 9
Ten:      DW 10
Fifty:    DW 50
X1111:    DW &H1110
FFFF:     DW &HFFFF
Forward:  DW 100
Reverse:  DW -100
EnSonar0: DW &B00000001
EnSonar1: DW &B00000010
EnSonar2: DW &B00000100
EnSonar3: DW &B00001000
EnSonar4: DW &B00010000
EnSonar5: DW &B00100000
EnSonar6: DW &B01000000
EnSonar7: DW &B10000000
EnSonars: DW &B11111111
ErrorIO:  DW &H9990
Sample:   DW &H0AAA
Prompt:   DW &H0ABC

; IO address space map
SWITCHES: EQU &H00  ; slide switches
LEDS:     EQU &H01  ; red LEDs
TIMER:    EQU &H02  ; timer, usually running at 10 Hz
XIO:      EQU &H03  ; pushbuttons and some misc. I/0
SEVENSEG: EQU &H04  ; seven-segment display (4-digits only)
LCD:      EQU &H06  ; primitive 4-digit LCD display
GLEDS:	  EQU &H08
LPOS:     EQU &H80  ; left wheel encoder position (read only)
LVEL:     EQU &H82  ; current left wheel velocity (read only)
LVELCMD:  EQU &H83  ; left wheel velocity command (write only)
RPOS:     EQU &H88  ; same values for right wheel...
RVEL:     EQU &H8A  ; ...
RVELCMD:  EQU &H8B  ; ...
I2C_CMD:  EQU &H90  ; I2C module's CMD register,
I2C_DATA: EQU &H91  ; ... DATA register,
I2C_BUSY: EQU &H92  ; ... and BUSY register
;SONAR:    EQU &HA0  ; base address for more than 16 registers....
DIST0:    EQU &HA8  ; the eight sonar distance readings
DIST1:    EQU &HA9  ; ...
DIST2:    EQU &HAA  ; ...
DIST3:    EQU &HAB  ; ...
DIST4:    EQU &HAC  ; ...
DIST5:    EQU &HAD  ; ...
DIST6:    EQU &HAE  ; ...
DIST7:    EQU &HAF  ; ...
SONAREN:  EQU &HB2  ; register to control which sonars are enabled
XPOS:     EQU &HC0  ; Current X-position (read only)
YPOS:     EQU &HC1  ; Y-position
THETA:    EQU &HC2  ; Current rotational position of robot (0-701)


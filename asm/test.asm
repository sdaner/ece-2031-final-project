	ORG     &H000		;Begin program at x000
TestInOut:
	LOAD	FFFF
	OUT		LEDS
	LOAD	ZERO
	IN		LEDS
	SUB		FFFF
	JZERO	IOGood
	LOAD	ErrorIO
	OUT		SEVENSEG
IOGood:
	LOAD	ZERO
	OUT		TIMER
Flash:
	IN		FFFF
	OUT		LEDS
	LOAD	Ten
	STORE	WaitTime
	CALL	Wait
FinishFlash:
	LOAD	Zero
	OUT		LEDS
	LOAD	Two
	STORE	WaitTime
	LOAD	Zero
TestDisplays:
	;0
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;1
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;2
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;3
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;4
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;5
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;6
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;7
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;8
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;9
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;A
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;B
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;C
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;D
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;E
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	;F
	OUT		SEVENSEG
	OUT		LCD
	ADD		X1111
	CALL	Wait
	JUMP	Main
Main:
	LOAD	Prompt
	OUT		LCD
	LOAD	Ten
	STORE	WaitTime
	CALL	Wait
	IN		Switches
	SUB		One
	JZERO   SampleInDepthTest
	JUMP	Done
SampleInDepthTest:
	LOAD	Sample
	OUT		LCD
Done:
	JUMP	Done
	
;===============================Wait functions==================
; Input: Store WaitTime (in tenths of a second)
Wait:
	STORE	WaitStak
	OUT		Timer
WaitCont:
	IN		Timer
	SUB		WaitTime
	JZERO	DoneWait
	JUMP	WaitCont
DoneWait:
	LOAD	WaitStak
	RETURN


; This is a good place to put variables
Temp:     DW 0
TestLED:  DW &H0
TestHex:  DW &H8888
WaitTime: DW &H01
WaitStak: DW 0

; Having some constants can be very useful
Zero:     DW 0
One:      DW 1
Two:      DW 2
Three:    DW 3
Four:     DW 4
Five:     DW 5
Six:      DW 6
Seven:    DW 7
Eight:    DW 8
Nine:     DW 9
Ten:      DW 10
Fifty:    DW 50
X1111:	  DW &H1110
FFFF:	  DW &HFFFF
Forward:  DW 100
Reverse:  DW -100
EnSonar0: DW &B00000001
EnSonar1: DW &B00000010
EnSonar2: DW &B00000100
EnSonar3: DW &B00001000
EnSonar4: DW &B00010000
EnSonar5: DW &B00100000
EnSonar6: DW &B01000000
EnSonar7: DW &B10000000
EnSonars: DW &B11111111
ErrorIO:  DW &H9990
Sample:	  DW &H0AAA
Prompt:	  DW &H0ABC

; IO address space map
SWITCHES: EQU &H00  ; slide switches
LEDS:     EQU &H01  ; red LEDs
TIMER:    EQU &H02  ; timer, usually running at 10 Hz
XIO:      EQU &H03  ; pushbuttons and some misc. I/0
SEVENSEG: EQU &H04  ; seven-segment display (4-digits only)
LCD:      EQU &H06  ; primitive 4-digit LCD display
LPOS:     EQU &H80  ; left wheel encoder position (read only)
LVEL:     EQU &H82  ; current left wheel velocity (read only)
LVELCMD:  EQU &H83  ; left wheel velocity command (write only)
RPOS:     EQU &H88  ; same values for right wheel...
RVEL:     EQU &H8A  ; ...
RVELCMD:  EQU &H8B  ; ...
I2C_CMD:  EQU &H90  ; I2C module's CMD register,
I2C_DATA: EQU &H91  ; ... DATA register,
I2C_BUSY: EQU &H92  ; ... and BUSY register
SONAR:    EQU &HA0  ; base address for more than 16 registers....
DIST0:    EQU &HA8  ; the eight sonar distance readings
DIST1:    EQU &HA9  ; ...
DIST2:    EQU &HAA  ; ...
DIST3:    EQU &HAB  ; ...
DIST4:    EQU &HAC  ; ...
DIST5:    EQU &HAD  ; ...
DIST6:    EQU &HAE  ; ...
DIST7:    EQU &HAF  ; ...
SONAREN:  EQU &HB2  ; register to control which sonars are enabled
XPOS:     EQU &HC0  ; Current X-position (read only)
YPOS:     EQU &HC1  ; Y-position
THETA:    EQU &HC2  ; Current rotational position of robot (0-701)
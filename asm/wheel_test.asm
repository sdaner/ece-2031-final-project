			ORG 	&H000
MAIN:
			IN		LVEL
			STORE   TEMP
			IN		RVEL
			ADD		TEMP
			STORE   TEMP
			JZERO 	ZERO 
			JUMP	TURNING
ZERO:		
			LOAD	LEDOFF
			OUT 	LEDS
			JUMP 	MAIN
TURNING:
			LOAD 	TEMP
			OUT		LCD
			LOAD 	LWLED
			OUT	 	LEDS
			JUMP 	MAIN
			
			
			ORG &H020 ; Start data  at x100
LPOS:		EQU &H80  ; left wheel encoder position (read only)
LVEL:		EQU &H82  ; current left wheel velocity (read only)
RPOS:		EQU &H88  ; same values for right wheel...
RVEL:		EQU &H8A  ; ...
LCD:      	EQU &H06  ; primitive 4-digit LCD display
SEVENSEG:	EQU &H04  ; seven-segment display (4-digits only)
SWITCHES: 	EQU &H00  ; slide switches
LEDOFF:		DW	0
LWLED:		DW	1	  ; left led is the first led
LEDS:     	EQU &H01  ; red LEDs
TEMP: 		DW  0
SW5MASK:	DW  32	  ; 32 is value of switch 5
